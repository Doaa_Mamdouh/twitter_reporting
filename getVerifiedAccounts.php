<?php
require_once('TweetPHP.php');
$config = include("config.php");

ini_set('display_errors', 1);
//error_reporting(E_ALL);
error_reporting(E_ERROR | E_PARSE); //Hide warnings
ini_set('max_execution_time', 3000000);

date_default_timezone_set('Asia/Riyadh');
$servername = $config['servername'] ;
$username = $config['username'];
$password = $config['password'] ;
$databaseName = $config['database_name'] ;

$conn = new PDO("mysql:host=$servername;dbname=$databaseName;charset=utf8", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$no_of_friends_requests = 0;

function get_org_info($screen_name){
	echo "screen name: " . $screen_name . "\n";
	
	global $no_of_friends_requests;
	$no_of_friends_requests ++;
	if($no_of_friends_requests % 800 == 0){
		sleep(900);
	}
	$TweetPHP = new TweetPHP(array(
			'consumer_key'        => 'wNTl6HZ64HDGqKtLb5OXYdgQs',
			'consumer_secret'     => 'Mzl37LAVNPwbh9Spn6Nf47FM064ba9HWGPN2nUIfSvLauKni9S',
			'access_token'        => '101179524-Xbye15nWe5GcpkWDRbMb8CvwynV4KmObRiVkB6WS',
			'access_token_secret' => 'TZqvZ1dgGVYAlZZknb32XPgw23cFoqmStjQDwe9t6ePgR',
			'api_endpoint'        => 'users/show',
			'api_params'          => array('screen_name' => $screen_name, 'tweet_mode' => 'extended')
	));
	$user_array = $TweetPHP->get_tweet_array();
	$verified = !empty($user_array['verified'])? 1 : 0;
	
	update_DB($screen_name, $verified);
	
}

function update_DB($screen_name, $verified){
	try{
		$conn = $GLOBALS['conn'];
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare("UPDATE verified_accounts set verified = :verified where account_screen_name = '$screen_name'");
		$stmt->bindParam(':verified', $verified);
		$stmt->execute();
		
	} catch (PDOException $e){
		echo $e->getMessage() . "\n";
	}
}

function run_for_all_accounts(){
	
	$conn = $GLOBALS['conn'];
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt = $conn->prepare("SELECT * FROM verified_accounts where verified = 0");
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$accounts = $stmt->fetchAll();
	
	$stmt2 = $conn->prepare("INSERT INTO cron_jobs_start_time(cron_job_name, start_time) values ('getVerifiedAccounts', '". date('Y-m-d H:i:s') ."')");
	$stmt2->execute();
	
	foreach ($accounts as $account){
		$account_screen_name = $account['account_screen_name'];
		get_org_info($account_screen_name); 
	}
}

run_for_all_accounts(); // 900 request.


?>