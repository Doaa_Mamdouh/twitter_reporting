<?php
require_once('TweetPHP.php');
$config = include("config.php");

ini_set('display_errors', 1);
//error_reporting(E_ALL);
error_reporting(E_ERROR | E_PARSE); //Hide warnings
ini_set('max_execution_time', 3000000);

date_default_timezone_set('Asia/Riyadh');
$servername = $config['servername'] ;
$username = $config['username'];
$password = $config['password'] ;
$databaseName = $config['database_name'] ;

$conn = new PDO("mysql:host=$servername;dbname=$databaseName;charset=utf8", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

function get_counters($account_screen_name){
	echo "\n". "screen name: " . $account_screen_name. "\n";
	$conn = $GLOBALS['conn'];
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	$counters_start_dates_array = array();
	$stmt = $conn->prepare("SELECT * FROM counters_start_dates");
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$counters_start_dates = $stmt->fetchAll();
	foreach ($counters_start_dates as $record){
		$counters_start_dates_array[$record['counter_name']] = $record['start_date'];
	}
	
	//print_r($counters_start_dates_array);
	
	//OT1 : Number of original tweets posted by organization.
	$OT1_start_date = $counters_start_dates_array['OT1'];
	$condition = (isset($OT1_start_date)) ? " and publish_date >= '". $OT1_start_date ."'": "";
	$stmt1 = $conn->prepare("SELECT COUNT(*) AS OT1 FROM tweets WHERE owner_screen_name LIKE '". $account_screen_name."'". $condition);
	$stmt1->execute();
	$stmt1->setFetchMode(PDO::FETCH_ASSOC);
	$tweets = $stmt1->fetchAll();
	if(isset($tweets[0]['OT1']))
		$OT1 = $tweets[0]['OT1'];
	else 
		$OT1 = 0;
		
	//RP1 : Number of replies posted by the organization.
	$RP1_start_date = $counters_start_dates_array['RP1'];
	$condition = (isset($RP1_start_date)) ? " and publish_date >= '". $RP1_start_date."'": "";
	$stmt2 = $conn->prepare("SELECT COUNT(*) AS RP1 FROM replies WHERE owner_screen_name LIKE '". $account_screen_name."'". $condition);
	$stmt2->execute();
	$stmt2->setFetchMode(PDO::FETCH_ASSOC);
	$replies = $stmt2->fetchAll();
	if(isset($replies[0]['RP1']))
		$RP1 = $replies[0]['RP1'];
	else
		$RP1= 0;
	
	//RP2 : Number of OTs posted by the organization and replied by other users.
	$RP2_start_date = $counters_start_dates_array['RP2'];
	$condition = (isset($RP2_start_date)) ? " and publish_date >= '". $RP2_start_date."'": "";
	$stmt3 = $conn->prepare("SELECT COUNT(*) AS RP2 FROM tweets WHERE owner_screen_name LIKE '". $account_screen_name."'". $condition ." AND has_replies = 1");
	$stmt3->execute();
	$stmt3->setFetchMode(PDO::FETCH_ASSOC);
	$tweets = $stmt3->fetchAll();
	if(isset($tweets[0]['RP2']))
		$RP2 = $tweets[0]['RP2'];
	else
		$RP2 = 0;
	
	//RP3 : Number of users who have replied organization’s tweets.
	$RP3_start_date = $counters_start_dates_array['RP3'];
	$condition = (isset($RP3_start_date)) ? " and created_at >= '". $RP3_start_date."'": "";
	$stmt4 = $conn->prepare("SELECT DISTINCT owner_screen_name FROM mentions_to_org WHERE org_screen_name LIKE '". $account_screen_name."'". $condition ." AND type = 'RP2'");
	$stmt4->execute();
	$stmt4->setFetchMode(PDO::FETCH_ASSOC);
	$owner_screen_names = $stmt4->fetchAll();
	if(isset($owner_screen_names))
		$RP3 = count($owner_screen_names);
	else
		$RP3 = 0;
	
	//RT1 : Number of retweets accomplished by the organization.
	$RT1_start_date = $counters_start_dates_array['RT1'];
	$condition = (isset($RT1_start_date)) ? " and publish_date >= '". $RT1_start_date."'": "";
	$stmt5 = $conn->prepare("SELECT COUNT(*) AS RT1 FROM retweets WHERE owner_screen_name LIKE '". $account_screen_name."'". $condition);
	$stmt5->execute();
	$stmt5->setFetchMode(PDO::FETCH_ASSOC);
	$retweets = $stmt5->fetchAll();
	if(isset($retweets[0]['RT1']))
		$RT1 = $retweets[0]['RT1'];
	else
		$RT1 = 0;
	
	//RT2 : Number of OTs posted by the organization and retweeted by other users.
	$RT2_start_date = $counters_start_dates_array['RT2'];
	$condition = (isset($RT2_start_date)) ? " and publish_date >= '". $RT2_start_date."'": "";
	$stmt6 = $conn->prepare("SELECT COUNT(*) AS RT2 FROM tweets WHERE owner_screen_name LIKE '". $account_screen_name."'". $condition ." AND retweet_count > 0");
	$stmt6->execute();
	$stmt6->setFetchMode(PDO::FETCH_ASSOC);
	$tweets = $stmt6->fetchAll();
	if(isset($tweets[0]['RT2']))
		$RT2 = $tweets[0]['RT2'];
	else
		$RT2 = 0;
	
	//RT2+ : Number of OTs posted by the organization and retweeted by other users.
	$RT2_start_date = $counters_start_dates_array['RT2'];
	$condition = (isset($RT2_start_date)) ? " and publish_date >= '". $RT2_start_date."'": "";
	$stmt6_1 = $conn->prepare("SELECT COUNT(*) AS RT2_plus FROM tweets WHERE owner_screen_name LIKE '". $account_screen_name."'". $condition ." AND retweet_count_plus > 0");
	$stmt6_1->execute();
	$stmt6_1->setFetchMode(PDO::FETCH_ASSOC);
	$tweets = $stmt6_1->fetchAll();
	if(isset($tweets[0]['RT2_plus']))
		$RT2_plus = $tweets[0]['RT2_plus'];
	else
		$RT2_plus = 0;
	
	//RT3 : Number of users who have retweeted organization’s tweets.
	$RT3_start_date = $counters_start_dates_array['RT3'];
	$condition = (isset($RT3_start_date)) ? " and publish_date >= '". $RT3_start_date."'": "";
	$stmt7 = $conn->prepare("SELECT SUM(retweet_count) AS RT3 FROM tweets WHERE owner_screen_name LIKE '". $account_screen_name."'". $condition ." AND retweet_count > 0");
	$stmt7->execute();
	$stmt7->setFetchMode(PDO::FETCH_ASSOC);
	$tweets = $stmt7->fetchAll();
	if(isset($tweets[0]['RT3']))
		$RT3 = $tweets[0]['RT3'];
	else
		$RT3 = 0;
	
	//RT3+ : Number of users who have retweeted organization’s tweets.
	$RT3_start_date = $counters_start_dates_array['RT3'];
	$condition = (isset($RT3_start_date)) ? " and publish_date >= '". $RT3_start_date."'": "";
	$stmt7_1 = $conn->prepare("SELECT SUM(retweet_count_plus) AS RT3_plus FROM tweets WHERE owner_screen_name LIKE '". $account_screen_name."'". $condition ." AND retweet_count_plus > 0");
	$stmt7_1->execute();
	$stmt7_1->setFetchMode(PDO::FETCH_ASSOC);
	$tweets = $stmt7_1->fetchAll();
	if(isset($tweets[0]['RT3_plus']))
		$RT3_plus = $tweets[0]['RT3_plus'];
	else
		$RT3_plus= 0;
	
	//FT2 : Number of organization’s tweets marked as favorite (liked) by other users.
	$FT2_start_date = $counters_start_dates_array['FT2'];
	$condition = (isset($FT2_start_date)) ? " and publish_date >= '". $FT2_start_date."'": "";
	$stmt8 = $conn->prepare("SELECT COUNT(*) AS FT2 FROM tweets WHERE owner_screen_name LIKE '". $account_screen_name."'". $condition ." AND favorite_count > 0");
	$stmt8->execute();
	$stmt8->setFetchMode(PDO::FETCH_ASSOC);
	$tweets = $stmt8->fetchAll();
	if(isset($tweets[0]['FT2']))
		$FT2 = $tweets[0]['FT2'];
	else
		$FT2 = 0;
	
	//FT2+ : Number of organization’s tweets marked as favorite (liked) by other users.
	$FT2_start_date = $counters_start_dates_array['FT2'];
	$condition = (isset($FT2_start_date)) ? " and publish_date >= '". $FT2_start_date."'": "";
	$stmt8_1 = $conn->prepare("SELECT COUNT(*) AS FT2_plus FROM tweets WHERE owner_screen_name LIKE '". $account_screen_name."'". $condition ." AND favorite_count_plus > 0");
	$stmt8_1->execute();
	$stmt8_1->setFetchMode(PDO::FETCH_ASSOC);
	$tweets = $stmt8_1->fetchAll();
	if(isset($tweets[0]['FT2_plus']))
		$FT2_plus = $tweets[0]['FT2_plus'];
	else
		$FT2_plus = 0;
	
	//FT3 : Number of users that have marked organization’s tweets as favorite (likes).
	$FT3_start_date = $counters_start_dates_array['FT3'];
	$condition = (isset($FT3_start_date)) ? " and publish_date >= '". $FT3_start_date."'": "";
	$stmt9 = $conn->prepare("SELECT SUM(favorite_count) AS FT3 FROM tweets WHERE owner_screen_name LIKE '". $account_screen_name."'". $condition ." AND favorite_count > 0");
	$stmt9->execute();
	$stmt9->setFetchMode(PDO::FETCH_ASSOC);
	$users = $stmt9->fetchAll();
	if(isset($users[0]['FT3']))
		$FT3 = $users[0]['FT3'];
	else
		$FT3 = 0;
	
	//FT3+ : Number of users that have marked organization’s tweets as favorite (likes).
	$FT3_start_date = $counters_start_dates_array['FT3'];
	$condition = (isset($FT3_start_date)) ? " and publish_date >= '". $FT3_start_date."'": "";
	$stmt9_1 = $conn->prepare("SELECT SUM(favorite_count_plus) AS FT3_plus FROM tweets WHERE owner_screen_name LIKE '". $account_screen_name."'". $condition ." AND favorite_count_plus > 0");
	$stmt9_1->execute();
	$stmt9_1->setFetchMode(PDO::FETCH_ASSOC);
	$users = $stmt9_1->fetchAll();
	if(isset($users[0]['FT3_plus']))
		$FT3_plus = $users[0]['FT3_plus'];
	else
		$FT3_plus = 0;
	
	//M1 : Number of mentions to other users by the organization.
	$M1_start_date = $counters_start_dates_array['M1'];
	$condition = (isset($M1_start_date)) ? " and publish_date >= '". $M1_start_date."'": "";
	$stmt10 = $conn->prepare("SELECT SUM(user_mentions) AS user_mentions_1 FROM tweets WHERE owner_screen_name LIKE '". $account_screen_name."'". $condition ." AND user_mentions > 0");
	$stmt10->execute();
	$stmt10->setFetchMode(PDO::FETCH_ASSOC);
	$mentions = $stmt10->fetchAll();
	if(isset($mentions[0]['user_mentions_1']))
		$user_mentions_1 = $mentions[0]['user_mentions_1'];
	else
		$user_mentions_1 = 0;
	
	$stmt11 = $conn->prepare("SELECT SUM(user_mentions) AS user_mentions_2 FROM retweets WHERE owner_screen_name LIKE '". $account_screen_name."'". $condition ." AND user_mentions > 0");
	$stmt11->execute();
	$stmt11->setFetchMode(PDO::FETCH_ASSOC);
	$mentions = $stmt11->fetchAll();
	if(isset($mentions[0]['user_mentions_2']))
		$user_mentions_2 = $mentions[0]['user_mentions_2'];
	else
		$user_mentions_2 = 0;
	
	$M1 = $user_mentions_1 + $user_mentions_2; 

	//M2 : Number of users mentioned by the organization.
	$M2_start_date = $counters_start_dates_array['M2'];
	$condition = (isset($M2_start_date)) ? " and publish_date >= '". $M2_start_date."'": "";
	$stmt12 = $conn->prepare("SELECT COUNT(DISTINCT mentioned_user_screen_name) AS count FROM users_mentioned_by_org 
			WHERE org_screen_name LIKE '". $account_screen_name."' ". $condition);
	$stmt12->execute();
	$stmt12->setFetchMode(PDO::FETCH_ASSOC);
	$mentions = $stmt12->fetchAll();
	if(isset($mentions[0]['count']))
		$M2 = $mentions[0]['count'];
	else
		$M2 = 0;
	
	//M3 : Number of mentions to the organization by other users
	$M3_start_date = $counters_start_dates_array['M3'];
	$condition = (isset($M3_start_date)) ? " and created_at >= '". $M3_start_date."'": "";
	$stmt15 = $conn->prepare("SELECT COUNT(*) AS M3 FROM mentions_to_org WHERE org_screen_name LIKE '". $account_screen_name."' ". $condition);
	$stmt15->execute();
	$stmt15->setFetchMode(PDO::FETCH_ASSOC);
	$mentions = $stmt15->fetchAll();
	if(isset($mentions[0]['M3']))
		$M3 = $mentions[0]['M3'];
	else
		$M3 = 0;
	
	//M4 : Number of users mentioning the organization.
	$M4_start_date = $counters_start_dates_array['M4'];
	$condition = (isset($M4_start_date)) ? " and created_at >= '". $M4_start_date."'": "";
	$stmt16 = $conn->prepare("SELECT COUNT(DISTINCT owner_id) AS M4 FROM mentions_to_org WHERE org_screen_name LIKE '". $account_screen_name."' ". $condition);
	$stmt16->execute();
	$stmt16->setFetchMode(PDO::FETCH_ASSOC);
	$users = $stmt16->fetchAll();
	if(isset($users[0]['M4']))
		$M4 = $users[0]['M4'];
	else
		$M4 = 0;
	
	//F2 : Number of verified Followers
	$stmt13 = $conn->prepare("SELECT COUNT(*) AS F2 FROM followers WHERE org_screen_name LIKE '". $account_screen_name."' AND verified = 1");
	$stmt13->execute();
	$stmt13->setFetchMode(PDO::FETCH_ASSOC);
	$followers = $stmt13->fetchAll();
	if(isset($followers[0]['F2']))
		$F2 = $followers[0]['F2'];
	else
		$F2 = 0;
	
	
	//F4 : Number of verified followees
	$stmt14 = $conn->prepare("SELECT COUNT(*) AS F4 FROM friends WHERE org_screen_name LIKE '". $account_screen_name."' AND verified = 1");
	$stmt14->execute();
	$stmt14->setFetchMode(PDO::FETCH_ASSOC);
	$followees = $stmt14->fetchAll();
	if(isset($followees[0]['F4']))
		$F4 = $followees[0]['F4'];
	else
		$F4 = 0;
	
	//TTR : avg in minutes
	$TTR_start_date = $counters_start_dates_array['TTR'];
	$condition = (isset($TTR_start_date)) ? " and publish_date >= '". $TTR_start_date."'": "";
	$stmt15 = $conn->prepare("SELECT AVG(TTR) AS TTR FROM replies WHERE owner_screen_name LIKE '". $account_screen_name."' ". $condition);
	$stmt15->execute();
	$stmt15->setFetchMode(PDO::FETCH_ASSOC);
	$TTRs = $stmt15->fetchAll();
	if(isset($TTRs[0]['TTR']))
		$TTR = $TTRs[0]['TTR'];
	else
		$TTR= 0;
	
	//M5 : Number of mentions to the organization by verified users.
	$M5_start_date = $counters_start_dates_array['M5'];
	$condition = (isset($M5_start_date)) ? " and created_at >= '". $M5_start_date."'": "";
	$stmt16 = $conn->prepare("SELECT COUNT(*) AS M5 FROM mentions_to_org WHERE org_screen_name LIKE '". $account_screen_name."'". $condition ." AND verified = 1");
	$stmt16->execute();
	$stmt16->setFetchMode(PDO::FETCH_ASSOC);
	$mentions_verified = $stmt16->fetchAll();
	if(isset($mentions_verified[0]['M5']))
		$M5 = $mentions_verified[0]['M5'];
	else
		$M5 = 0;
	
	//M6 : Number of mentions to the organization by unverified users.
	$M6_start_date = $counters_start_dates_array['M6'];
	$condition = (isset($M6_start_date)) ? " and created_at >= '". $M6_start_date."'": "";
	$stmt17 = $conn->prepare("SELECT COUNT(*) AS M6 FROM mentions_to_org WHERE org_screen_name LIKE '". $account_screen_name."'". $condition ." AND verified = 0");
	$stmt17->execute();
	$stmt17->setFetchMode(PDO::FETCH_ASSOC);
	$mentions_verified = $stmt17->fetchAll();
	if(isset($mentions_verified[0]['M6']))
		$M6 = $mentions_verified[0]['M6'];
	else
		$M6 = 0;
			
	//RP4 : Number of replies to organization’s tweets by verified users.
	$RP4_start_date = $counters_start_dates_array['RP4'];
	$condition = (isset($RP4_start_date)) ? " and created_at >= '". $RP4_start_date."'": "";
	$stmt18 = $conn->prepare("SELECT COUNT(*) AS RP4 FROM mentions_to_org WHERE org_screen_name LIKE '". $account_screen_name."'". $condition ." AND type = 'RP2' AND verified = 1");
	$stmt18->execute();
	$stmt18->setFetchMode(PDO::FETCH_ASSOC);
	$mentions_verified = $stmt18->fetchAll();
	if(isset($mentions_verified[0]['RP4']))
		$RP4 = $mentions_verified[0]['RP4'];
	else
		$RP4 = 0;
		
	//RP5 : Number of replies to organization’s tweets by unverified users.
	$RP5_start_date = $counters_start_dates_array['RP5'];
	$condition = (isset($RP5_start_date)) ? " and created_at >= '". $RP5_start_date."'": "";
	$stmt18 = $conn->prepare("SELECT COUNT(*) AS RP5 FROM mentions_to_org WHERE org_screen_name LIKE '". $account_screen_name."'". $condition ." AND type = 'RP2' AND verified = 0");
	$stmt18->execute();
	$stmt18->setFetchMode(PDO::FETCH_ASSOC);
	$mentions_verified = $stmt18->fetchAll();
	if(isset($mentions_verified[0]['RP5']))
		$RP5 = $mentions_verified[0]['RP5'];
	else
		$RP5 = 0;
		
	//RT4 : Number of retweets to organization’s tweets by verified users.
	$RT4_start_date = $counters_start_dates_array['RT4'];
	$condition = (isset($RT4_start_date)) ? " and created_at >= '". $RT4_start_date."'": "";
	$stmt18 = $conn->prepare("SELECT COUNT(*) AS RT4 FROM mentions_to_org WHERE org_screen_name LIKE '". $account_screen_name."'". $condition ." AND type = 'retweet' AND verified = 1");
	$stmt18->execute();
	$stmt18->setFetchMode(PDO::FETCH_ASSOC);
	$mentions_verified = $stmt18->fetchAll();
	if(isset($mentions_verified[0]['RT4']))
		$RT4 = $mentions_verified[0]['RT4'];
	else
		$RT4 = 0;
		
	//RT5 : Number of retweets to organization’s tweets by unverified users.
	$RT5_start_date = $counters_start_dates_array['RT5'];
	$condition = (isset($RT5_start_date)) ? " and created_at >= '". $RT5_start_date."'": "";
	$stmt18 = $conn->prepare("SELECT COUNT(*) AS RT5 FROM mentions_to_org WHERE org_screen_name LIKE '". $account_screen_name."'". $condition ." AND type = 'retweet' AND verified = 0");
	$stmt18->execute();
	$stmt18->setFetchMode(PDO::FETCH_ASSOC);
	$mentions_verified = $stmt18->fetchAll();
	if(isset($mentions_verified[0]['RT5']))
		$RT5 = $mentions_verified[0]['RT5'];
	else
		$RT5 = 0;
			
	//print_r($owner_screen_names);
	$counters_array = array(
			'user_screen_name' => $account_screen_name,
			'OT1' => $OT1,
			'RP1' => $RP1,
			'RP2' => $RP2,
			'RP3' => $RP3,
			'RT1' => $RT1,
			'RT2' => $RT2,
			'RT2_plus' => $RT2_plus,
			'RT3' => $RT3,
			'RT3_plus' => $RT3_plus,
			'FT2' => $FT2,
			'FT2_plus' => $FT2_plus,
			'FT3' => $FT3,
			'FT3_plus' => $FT3_plus,
			'M1' => $M1,
			'M2' => $M2,
			'M3' => $M3,
			'M4' => $M4,
			'F2' => $F2,
			'F4' => $F4,
			'TTR' => $TTR,
			'M5' => $M5,
			'M6' => $M6,
			'RP4' => $RP4,
			'RP5' => $RP5,
			'RT4' => $RT4,
			'RT5' => $RT5,
			
	);
	update_org_counters($counters_array);
	
}

function update_org_counters($item){
	try{
		$conn = $GLOBALS['conn'];
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$stmt1 = $conn->prepare("SELECT * FROM org_counters WHERE user_screen_name LIKE '". $item['user_screen_name'] . "'");
		$stmt1->execute();
		$stmt1->setFetchMode(PDO::FETCH_ASSOC);
		$org_counters= $stmt1->fetchAll();
		
		if(isset($org_counters[0]['id']))
		{
			$user_id = $org_counters[0]['id'];
			$stmt = $conn->prepare("UPDATE org_counters set OT1 = :OT1, RP1 = :RP1, RP2 = :RP2, RP3 = :RP3, RT1 = :RT1, RT2 = :RT2, RT2_plus = :RT2_plus, RT3 = :RT3, RT3_plus = :RT3_plus,
					FT2 = :FT2, FT2_plus = :FT2_plus, FT3 = :FT3, FT3_plus = :FT3_plus, M1 = :M1, M2 = :M2, M3 = :M3, M4 = :M4, F2 = :F2, F4 = :F4, TTR = :TTR,
					M5 =:M5, M6 = :M6, RP4 = :RP4, RP5 = :RP5, RT4 = :RT4, RT5 = :RT5 where id = $user_id");
			
			$stmt->bindParam(':OT1', $item['OT1']);
			$stmt->bindParam(':RP1', $item['RP1']);
			$stmt->bindParam(':RP2', $item['RP2']);
			$stmt->bindParam(':RP3', $item['RP3']);
			$stmt->bindParam(':RT1', $item['RT1']);
			$stmt->bindParam(':RT2', $item['RT2']);
			$stmt->bindParam(':RT2_plus', $item['RT2_plus']);
			$stmt->bindParam(':RT3', $item['RT3']);
			$stmt->bindParam(':RT3_plus', $item['RT3_plus']);
			$stmt->bindParam(':FT2', $item['FT2']);
			$stmt->bindParam(':FT2_plus', $item['FT2_plus']);
			$stmt->bindParam(':FT3', $item['FT3']);
			$stmt->bindParam(':FT3_plus', $item['FT3_plus']);
			$stmt->bindParam(':M1', $item['M1']);
			$stmt->bindParam(':M2', $item['M2']);
			$stmt->bindParam(':M3', $item['M3']);
			$stmt->bindParam(':M4', $item['M4']);
			$stmt->bindParam(':F2', $item['F2']);
			$stmt->bindParam(':F4', $item['F4']);
			$stmt->bindParam(':TTR', $item['TTR']);
			$stmt->bindParam(':M5', $item['M5']);
			$stmt->bindParam(':M6', $item['M6']);
			$stmt->bindParam(':RP4', $item['RP4']);
			$stmt->bindParam(':RP5', $item['RP5']);
			$stmt->bindParam(':RT4', $item['RT4']);
			$stmt->bindParam(':RT5', $item['RT5']);
			$stmt->execute();
		} 
		
	} catch (PDOException $e){
		echo $e->getMessage() . "\n";
	}
}

function run_for_all_organizations(){
	/* $organizations = array(
			'Aurorastatisti1',
			'amazon',
			'Disney',
			'Starbucks',
			'Microsoft',
			'google',
			'netflix',
			'jpmorgan',
			'FedEx',
	); */
	
	$conn = $GLOBALS['conn'];
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt = $conn->prepare("SELECT * FROM org_screen_names");
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$organizations = $stmt->fetchAll();
	
	$stmt2 = $conn->prepare("INSERT INTO cron_jobs_start_time(cron_job_name, start_time) values ('getCounters', '". date('Y-m-d H:i:s') ."')");
	$stmt2->execute();
	
	foreach ($organizations as $org){
		$org_screen_name = $org['org_screen_name'];
		get_counters($org_screen_name);
	}
}

run_for_all_organizations(); 
//get_counters("RawanMakkii")


?>