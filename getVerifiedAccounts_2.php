<?php
require_once('TweetPHP.php');
$config = include("config.php");

ini_set('display_errors', 1);
//error_reporting(E_ALL);
error_reporting(E_ERROR | E_PARSE); //Hide warnings
ini_set('max_execution_time', 3000000);

date_default_timezone_set('Asia/Riyadh');
$servername = $config['servername'];
$username = $config['username'];
$password = $config['password'];
$databaseName = $config['database_name'];

$conn = new PDO("mysql:host=$servername;dbname=$databaseName;charset=utf8", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$no_of_requests = 0;

function get_org_info($account_user_name, $id){
	echo "\n". "account_user_name: " . $account_user_name. "\n";
	
	$account_user_name= urlencode($account_user_name);
	//echo "<br>account_user_name_encoded: $account_user_name<br>";
	//$search_terms = "\"". $account_user_name."\""; //Double quotes for containing the exact phrase.
	
	global $no_of_requests;
	$no_of_requests ++;
	if($no_of_requests % 800 == 0){
		sleep(900);
	}
	$TweetPHP = new TweetPHP(array(
			'consumer_key'        => 'wNTl6HZ64HDGqKtLb5OXYdgQs',
			'consumer_secret'     => 'Mzl37LAVNPwbh9Spn6Nf47FM064ba9HWGPN2nUIfSvLauKni9S',
			'access_token'        => '101179524-Xbye15nWe5GcpkWDRbMb8CvwynV4KmObRiVkB6WS',
			'access_token_secret' => 'TZqvZ1dgGVYAlZZknb32XPgw23cFoqmStjQDwe9t6ePgR',
			'api_endpoint'        => 'users/search',
			'api_params'          => array('q' => $account_user_name, 'count' => 1)
	));
	$user_array = $TweetPHP->get_tweet_array();
	print_r($user_array);
	if($user_array[0] != "Error fetching or loading tweets" && isset($user_array[0]['verified'])){
		$verified = !empty($user_array[0]['verified'])? 1 : 0;
	}else{
		$verified = -1;// -1 represents that we can't know the result.
	}
	update_DB($account_user_name, $verified, $id);
	echo "<br>verified: $verified<br>";
	echo "<br>no_of_requests: $no_of_requests <br>";
	echo "-------------------------------------------";
}

function update_DB($account_user_name, $verified, $id){
	try{
		$conn = $GLOBALS['conn'];
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare("UPDATE verified_accounts set verified = :verified where id = $id");
		$stmt->bindParam(':verified', $verified);
		$stmt->execute();
		
	} catch (PDOException $e){
		echo $e->getMessage() . "\n";
	}
}

function run_for_all_accounts(){
	
	$conn = $GLOBALS['conn'];
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn->exec("set names utf8mb4");
	
	$stmt = $conn->prepare("select * from verified_accounts where verified is null;");
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$accounts = $stmt->fetchAll();
	
	$stmt2 = $conn->prepare("INSERT INTO cron_jobs_start_time(cron_job_name, start_time) values ('getVerifiedAccounts', '". date('Y-m-d H:i:s') ."')");
	$stmt2->execute();
	
	foreach ($accounts as $account){
		$account_user_name = $account['account_user_name'];
		$id = $account['id'];
		get_org_info($account_user_name, $id); 
	}
}

run_for_all_accounts(); // 900 request.
//get_org_info("Women's Health"); 

?>