<?php
require_once('TweetPHP.php');
$config = include("config.php");

ini_set('display_errors', 1);
//error_reporting(E_ALL);
error_reporting(E_ERROR | E_PARSE); //Hide warnings
ini_set('max_execution_time', 3000000);

date_default_timezone_set('Asia/Riyadh');
$servername = $config['servername'] ;
$username = $config['username'];
$password = $config['password'] ;
$databaseName = $config['database_name'] ;

$conn = new PDO("mysql:host=$servername;dbname=$databaseName;charset=utf8", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

echo date() . "======= \n";
$no_of_requests = 0;

function get_ttr($account_screen_name){
	echo "\n". "screen name: " . $account_screen_name. "\n";
	
	global $no_of_requests;
	$conn = $GLOBALS['conn'];
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	$stmt1 = $conn->prepare("SELECT * FROM replies WHERE owner_screen_name LIKE '". $account_screen_name."' AND TTR is null limit 100");
	$stmt1->execute();
	$stmt1->setFetchMode(PDO::FETCH_ASSOC);
	$replies = $stmt1->fetchAll();
	
	foreach ($replies as $reply){
		$reply_id = $reply['reply_id'];
		$parent_tweet_id = $reply['parent_tweet_id'];
		$reply_created_at = $reply['created_at'];
		
		$tweet_created_at = null;
		//1. search first in mentions_to_org 
		/* $stmt2 = $conn->prepare("SELECT created_at FROM mentions_to_org WHERE org_screen_name LIKE '". $account_screen_name."' AND tweet_id = '". $parent_tweet_id ."'");
		$stmt2->execute();
		$stmt2->setFetchMode(PDO::FETCH_ASSOC);
		$mentions = $stmt2->fetchAll();
		if(isset($mentions[0]['created_at']))
		{
			$tweet_created_at = $mentions[0]['created_at'];
		}else{ */
			//2. request a specific tweet
			$no_of_requests ++;
			if($no_of_requests % 900 == 0){ //max number of requests is 900 / 15min
				sleep(900);
			} 
			$tweet_created_at = get_tweet_details($parent_tweet_id);
		//}
		
		if($tweet_created_at != null){
			$TTR = floor((strtotime($reply_created_at) - strtotime($tweet_created_at))/ (60)); // The time difference in minutes.
			$stmt3 = $conn->prepare("UPDATE replies set TTR = $TTR where reply_id = $reply_id");
			$stmt3->execute();
		}
	}
}

function get_tweet_details($tweet_id){
	$TweetPHP = new TweetPHP(array(
			'consumer_key'        => 'wNTl6HZ64HDGqKtLb5OXYdgQs',
			'consumer_secret'     => 'Mzl37LAVNPwbh9Spn6Nf47FM064ba9HWGPN2nUIfSvLauKni9S',
			'access_token'        => '101179524-Xbye15nWe5GcpkWDRbMb8CvwynV4KmObRiVkB6WS',
			'access_token_secret' => 'TZqvZ1dgGVYAlZZknb32XPgw23cFoqmStjQDwe9t6ePgR',
			'api_endpoint'        => 'statuses/show',
			'api_params'          => array('id' => $tweet_id, 'tweet_mode' => 'extended')
	));
	$tweet_array = $TweetPHP->get_tweet_array();
	$tweet_created_at = $tweet_array['created_at'];
	
	if(!isset($tweet_created_at))
		return 0;
	return $tweet_created_at;
}

function get_cron_job_state(){
	$conn = $GLOBALS['conn'];
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	$stmt = $conn->prepare("SELECT get_ttr FROM cron_jobs");
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$get_ttr = $stmt->fetchAll();
	$cron_running = $get_ttr[0]['get_ttr'];
	if($cron_running == "Y")
		return true;
	else
		return false;
}

function update_cron_jobs_table($value){
	try {
		$conn = $GLOBALS['conn'];
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$stmt = $conn->prepare("UPDATE cron_jobs set get_ttr = '$value'");
		$stmt->execute();
	} catch (PDOException $e){
		echo $e->getMessage() . "\n";
	}
}


function run_for_all_organizations(){
	try {
		/* $organizations = array(
				'Aurorastatisti1',
				'FedEx',
				'jpmorgan',
				'amazon',
				'Disney',
				'netflix',
				'Microsoft',
				'Starbucks',
				'google', 
		); */
		$cron_running = get_cron_job_state();
		if(!$cron_running){
			update_cron_jobs_table("Y");
			
			$conn = $GLOBALS['conn'];
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$stmt = $conn->prepare("select * from org_screen_names order by id desc");
			$stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$organizations = $stmt->fetchAll();
			
			$stmt2 = $conn->prepare("INSERT INTO cron_jobs_start_time(cron_job_name, start_time) values ('get_TTR', '". date('Y-m-d H:i:s') ."')");
			$stmt2->execute();
			
			foreach ($organizations as $org){
				$org_screen_name = $org['org_screen_name'];
				get_ttr($org_screen_name);
			}
			update_cron_jobs_table("N");
		}
	} catch (PDOException $e){
		echo $e->getMessage() . "\n";
	}
}

run_for_all_organizations(); 
//get_ttr("amazon")


?>